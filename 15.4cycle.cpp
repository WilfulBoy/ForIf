﻿#include <iostream>

void PrintNumbers(int N, bool isOdd)
{
    if (isOdd)
    {
        for (int u = 0; u <= N; u++)
               {
                   if (u % 2 != isOdd)
                   {
                       std::cout << u << " ";
                   }
               }
    }
    else
    {
        for (int u = 0; u <= N; u++)
        {
            if (u % 2 != isOdd)
            {
                std::cout << u << " ";
            }
        }
    }
}

int main()
{
    PrintNumbers(20, true);
    PrintNumbers(20, false);
}






//int main()
//{
//   bool c = true;
//   int N = 12;
//   
//   for (int u = 0; u <= N; u++)
//   {
//       if (u % 2 != c)
//       {
//           std::cout << u << " ";
//       }
//   }
//}